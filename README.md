Created by Scott Greenup 2010

I was bored one evening and I decided to create this game using batch which I had recently taught myself.
I did not add in a check to see if anyone had won. =(

# Controls ####################################################################
Using QWE, ASD, ZXC as a grid which matches up to the onscreen grid:
	Q W E
	A S D
	Z X C
	
G is to reset the game