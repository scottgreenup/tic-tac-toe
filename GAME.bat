@echo off
COLOR 0F
set def=-
set player=one

GOTO reset

:draw
cls
echo Current player: %player%
echo.
echo [%Q%] [%W%] [%E%]
echo [%A%] [%S%] [%D%]
echo [%Z%] [%X%] [%C%]
echo.
GOTO update

:update
if not %Q%==%def% if not %W%==%def% if not %E%==%def% if not %A%==%def% if not %S%==%def% if not %D%==%def% if not %Z%==%def% if not %X%==%def% if not %C%==%def% (
echo.
pause
set Q=%def%
set W=%def%
set E=%def%
set A=%def%
set S=%def%
set D=%def%
set Z=%def%
set X=%def%
set C=%def%
GOTO draw
)

CHOICE /C:qweasdzxcg /N
if errorlevel 10 goto reset
if errorlevel 9 goto nine
if errorlevel 8 goto eight
if errorlevel 7 goto seven
if errorlevel 6 goto six
if errorlevel 5 goto five
if errorlevel 4 goto four
if errorlevel 3 goto three
if errorlevel 2 goto two
if errorlevel 1 goto one

:reset
set Q=%def%
set W=%def%
set E=%def%
set A=%def%
set S=%def%
set D=%def%
set Z=%def%
set X=%def%
set C=%def%
GOTO draw

:nine
if %player%==one (
set C=X
set player=two
GOTO draw
)
if %player%==two (
set C=O
set player=one
GOTO draw
)


:eight
if %player%==one (
set player=two
set X=X
GOTO draw
)
if %player%==two (
set player=one
set X=O
GOTO draw
)

:seven
if %player%==one (
set player=two
set Z=X
GOTO draw
)
if %player%==two (
set player=one
set Z=O
GOTO draw
)

:six
if %player%==one (
set player=two
set D=X
GOTO draw
)
if %player%==two (
set player=one
set D=O
GOTO draw
)

:five
if %player%==one (
set player=two
set S=X
GOTO draw
)
if %player%==two (
set player=one
set S=O
GOTO draw
)

:four
if %player%==one (
set player=two
set A=X
GOTO draw
)
if %player%==two (
set player=one
set A=O
GOTO draw
)

:three
if %player%==one (
set player=two
set E=X
GOTO draw
)
if %player%==two (
set player=one
set E=O
GOTO draw
)

:two
if %player%==one (
set player=two
set W=X
GOTO draw
)
if %player%==two (
set player=one
set W=O
GOTO draw
)

:one
if %player%==one (
set player=two
set Q=X
GOTO draw
)
if %player%==two (
set player=one
set Q=O
GOTO draw
)
